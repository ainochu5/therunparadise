package com.ainochu.mygame;

import com.ainochu.mygame.screens.SplashScreen;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class TheRunParadise extends Game {
	private Skin skin;
	public int score;
	public boolean paused;

	//LANZADERA
	@Override
	public void create() {
		setScreen(new SplashScreen(this));
	}

	//PASARLE EL ASPECTO DEL MENU A LOS BOTONES
	public Skin getSkin() {
		if(skin == null){
			//SI NO TIENES EL SKIN LE PASAMOS EL JSON
			skin = new Skin(Gdx.files.internal("ui/uiskin.json"));
		}
		return skin;
	}
}
