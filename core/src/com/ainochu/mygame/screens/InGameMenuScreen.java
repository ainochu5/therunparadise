package com.ainochu.mygame.screens;

import com.ainochu.mygame.TheRunParadise;
import com.ainochu.mygame.managers.ConfigurationManager;
import com.ainochu.mygame.managers.SpriteManager;
import com.ainochu.mygame.util.Constants;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import jdk.nashorn.internal.codegen.ClassEmitter;

public class InGameMenuScreen implements Screen {

	private Texture ImagenPauseScreen;
	TheRunParadise game;
	GameScreen gameScreen;
	Stage stage;
	private SpriteBatch batch;
	private SpriteManager spriteManager;
	Preferences prefs;

	public InGameMenuScreen(TheRunParadise game, GameScreen gameScreen, SpriteManager spriteManager) {
		this.game = game;
		this.gameScreen = gameScreen;
		batch = new SpriteBatch();
		this.spriteManager = spriteManager;
		ImagenPauseScreen = new Texture("img/pausaScreen.jpg");
	}
	
	private void loadScreen() {

		stage = new Stage();

		Table table = new Table();
		table.setPosition(Constants.SCREEN_WIDTH / 2.5f, Constants.SCREEN_HEIGTH / 1.5f);
	    table.setFillParent(true);
	    table.setHeight(500);
	    stage.addActor(table);
		
		Label label = new Label("", game.getSkin());
		table.addActor(label);

		final CheckBox checkSound = new CheckBox(" SOUND", game.getSkin());
		checkSound.setColor(Color.LIME);
		checkSound.setHeight(70);
		checkSound.setPosition(label.getOriginX(), label.getOriginY() - 250);
		checkSound.setChecked(prefs.getBoolean("sound"));
		checkSound.addListener(new ClickListener() {
			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {

				prefs.putBoolean("sound", checkSound.isChecked());
			}
		});

		table.addActor(checkSound);

		TextButton buttonResume = new TextButton("Continuar", game.getSkin());
		buttonResume.setPosition(label.getOriginX(), label.getOriginY() - 120);
		buttonResume.setWidth(200);
		buttonResume.setHeight(90);
		buttonResume.setColor(Color.PURPLE);
		buttonResume.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				return true;
			}

			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {

				dispose();
				if (ConfigurationManager.isSoundEnabled()) {
					spriteManager.music.play();
				}
				game.setScreen(gameScreen);
			}
		});
		table.addActor(buttonResume);
		
		TextButton buttonMainMenu = new TextButton("Volver al Menu Principal", game.getSkin());
		buttonMainMenu.setPosition(label.getOriginX()-300, label.getOriginY() - 240);
		buttonMainMenu.setWidth(200);
		buttonMainMenu.setColor(Color.LIME);
		buttonMainMenu.setHeight(90);
		buttonMainMenu.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				return true;
			}

			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {

				dispose();
				game.setScreen(new MainMenuScreen(game));
			}
		});
		table.addActor(buttonMainMenu);
		
		TextButton buttonQuit = new TextButton("Salir", game.getSkin());
		buttonQuit.setPosition(label.getOriginX()+300, label.getOriginY() - 240);
		buttonQuit.setWidth(200);
		buttonQuit.setColor(Color.CORAL);
		buttonQuit.setHeight(90);
		buttonQuit.addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				return true;	
			}
			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				
				dispose();
				System.exit(0);
			}
		});
		table.addActor(buttonQuit);
		
		Gdx.input.setInputProcessor(stage);
	}
	
	@Override
	public void show() {

		loadPreferences();
		loadScreen();
	}
	
	@Override
	public void render(float dt) {
	
		Gdx.gl.glClearColor(0, 0, 0, 0);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        
		// Pinta el menú
		stage.act(dt);
		batch.begin();
		batch.draw(ImagenPauseScreen,0,0,ImagenPauseScreen.getWidth(), ImagenPauseScreen.getHeight());
		batch.end();
		stage.draw();
	}
	
	@Override
	public void dispose() {
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {	
	}

	@Override
	public void resize(int arg0, int arg1) {
	}

	@Override
	public void resume() {		
	}

	private void loadPreferences() {

		prefs = Gdx.app.getPreferences(Constants.NOMBRE_JUEGO);

		// Coloca los valores por defecto (para la primera ejecución)
		if (!prefs.contains("sound"))
			prefs.putBoolean("sound", true);
	}
}
