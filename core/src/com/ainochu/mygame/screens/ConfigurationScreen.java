package com.ainochu.mygame.screens;

import com.ainochu.mygame.TheRunParadise;
import com.ainochu.mygame.managers.ResourceManager;
import com.ainochu.mygame.managers.SpriteManager;
import com.ainochu.mygame.util.Constants;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.*;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.Viewport;
import jdk.nashorn.internal.codegen.ClassEmitter;

import javax.swing.*;

/**
 * Created by Ainoa on 07/02/2016.
 */
public class ConfigurationScreen implements Screen {

    private Texture ImagenConfigurationScreen;
    TheRunParadise game;
    Sound sound;
    Stage stage;
    // Almacena las preferencias (en %UserProfile%/.prefs/PreferencesName)
    Preferences prefs;
    private SpriteBatch batch;

    public ConfigurationScreen(TheRunParadise game) {
        this.game = game;
        ImagenConfigurationScreen = new Texture("img/configuracion.jpg");
        batch = new SpriteBatch();

    }
    private void loadScreen() {

        ResourceManager.getSound(Constants.SOUND + "select_your_player.wav").play();

        stage = new Stage();

        Table table = new Table(game.getSkin());
        table.setPosition(Constants.SCREEN_WIDTH / 2.5f, Constants.SCREEN_HEIGTH / 1.5f);
        table.setFillParent(true);
        table.setHeight(500);
        stage.addActor(table);

        Label label = new Label("", game.getSkin());
        label.setColor(Color.RED);
        label.setFontScale(2.5f);
        table.addActor(label);

        final CheckBox checkSound = new CheckBox(" SOUND", game.getSkin());
        checkSound.setColor(Color.RED);
        checkSound.setChecked(prefs.getBoolean("sound"));
        checkSound.addListener(new ClickListener() {
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {

                prefs.putBoolean("sound", checkSound.isChecked());
            }
        });

        table.addActor(checkSound);

        Label difficultyLabel = new Label("-- PERSONAJES --", game.getSkin());
        difficultyLabel.setColor(Color.RED);
        difficultyLabel.setPosition(label.getOriginX(), label.getOriginY() - 120);
        difficultyLabel.setHeight(70);
        table.addActor(difficultyLabel);



        Texture textureVaquera   = new Texture(Gdx.files.internal("texturas/vaquera_idle_derecha.png"));
        Image vaqueraImage = new Image(textureVaquera);
        vaqueraImage.setPosition(label.getOriginX()-250, label.getOriginY() - 240);
        vaqueraImage.setWidth(textureVaquera.getWidth());
        vaqueraImage.setHeight(textureVaquera.getHeight());
        vaqueraImage.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                prefs.flush();
                dispose();
                game.setScreen(new InstructionScreen(game));
                com.badlogic.gdx.utils.Timer.schedule(new Timer.Task() {
                    @Override
                    public void run() {
                        game.setScreen(new GameScreen(game, "vaquera"));
                    }
                }, 4);
                return true;
            }
        });
        table.addActor(vaqueraImage);

        Texture textureRobot   = new Texture(Gdx.files.internal("texturas/robot_idle_derecha.png"));
        Image robotImage = new Image(textureRobot);
        robotImage.setPosition(label.getOriginX()+50, label.getOriginY() - 240);
        robotImage.setWidth(textureRobot.getWidth());
        robotImage.setHeight(textureRobot.getHeight());
        robotImage.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                prefs.flush();
                dispose();
                Constants.NOMBRE_JUGADOR = "robot";
                game.setScreen(new InstructionScreen(game));
                com.badlogic.gdx.utils.Timer.schedule(new Timer.Task() {
                    @Override
                    public void run() {
                        game.setScreen(new GameScreen(game, "robot"));
                    }
                }, 4);
                return true;
            }
        });
        table.addActor(robotImage);

        Texture textureNinja   = new Texture(Gdx.files.internal("texturas/ninja_idle_derecha.png"));
        Image ninjaImage = new Image(textureNinja);
        ninjaImage.setPosition(label.getOriginX()+300, label.getOriginY() - 240);
        ninjaImage.setWidth(74);
        ninjaImage.setHeight(128);
        ninjaImage.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                prefs.flush();
                dispose();
                Constants.NOMBRE_JUGADOR = "ninja";
                game.setScreen(new InstructionScreen(game));
                com.badlogic.gdx.utils.Timer.schedule(new Timer.Task() {
                    @Override
                    public void run() {
                        game.setScreen(new GameScreen(game, "ninja"));
                    }
                }, 4);
                return true;
            }
        });
        table.addActor(ninjaImage);


        TextButton exitButton = new TextButton("MAIN MENU", game.getSkin());
        exitButton.setPosition(label.getOriginX(), label.getOriginY() - 320);
        exitButton.setColor(Color.RED);
        exitButton.setWidth(200);
        exitButton.setHeight(70);
        exitButton.addListener(new ClickListener() {
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                prefs.flush();
                dispose();
                game.setScreen(new MainMenuScreen(game));
            }
        });

        table.addActor(exitButton);
        TextButton playButton = new TextButton("PLAY", game.getSkin());
        playButton.setPosition(label.getOriginX(), label.getOriginY() - 410);
        playButton.setColor(Color.PINK);
        playButton.setWidth(200);
        playButton.setHeight(70);
        playButton.addListener(new ClickListener() {
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                prefs.flush();
                dispose();
                Constants.NOMBRE_JUGADOR = "vaquera";
                game.setScreen(new InstructionScreen(game));
                com.badlogic.gdx.utils.Timer.schedule(new Timer.Task() {
                    @Override
                    public void run() {
                        game.setScreen(new GameScreen(game, "vaquera"));
                    }
                }, 4);

            }
        });

        table.addActor(playButton);


        Label aboutLabel = new Label("The run paradise\n(c) Ainoa Andres Burguete\nhttp://bitbucket.org/ainochu/TheRunParadise", game.getSkin());
        playButton.setPosition(label.getOriginX(), label.getOriginY() - 420);
        aboutLabel.setFontScale(1f);
        aboutLabel.setColor(Color.CYAN);

        Gdx.input.setInputProcessor(stage);
    }


    private void loadPreferences() {

        prefs = Gdx.app.getPreferences(Constants.NOMBRE_JUEGO);

        // Coloca los valores por defecto (para la primera ejecución)
        if (!prefs.contains("sound"))
            prefs.putBoolean("sound", true);
    }

    @Override
    public void show() {

        loadPreferences();
        loadScreen();
    }

    @Override
    public void render(float dt) {

        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(dt);
        batch.begin();
        batch.draw(ImagenConfigurationScreen, 0, 0, ImagenConfigurationScreen.getWidth(), ImagenConfigurationScreen.getHeight());
        batch.end();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.setViewport(stage.getViewport());
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

        stage.dispose();
    }
}
