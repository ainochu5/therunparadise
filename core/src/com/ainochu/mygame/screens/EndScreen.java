package com.ainochu.mygame.screens;

import com.ainochu.mygame.TheRunParadise;
import com.ainochu.mygame.managers.ConfigurationManager;
import com.ainochu.mygame.util.Constants;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.sun.prism.impl.shape.OpenPiscesRasterizer;

/**
 * Created by Ainoa on 06/03/2016.
 */
public class EndScreen implements Screen{

    private Texture ImagenMenuScreen;
    SpriteBatch batch;
    TheRunParadise game;
    Stage stage;
    boolean die;
    private Label label;
    private Texture ImagenGameOverScreen;
    private Texture ImagenGameEndScreen;
    private int score;

    public EndScreen(TheRunParadise game,boolean die,int score){
        this.game = game;
        batch = new SpriteBatch();
        this.die = die;
        this.score = score;

    }
    @Override
    public void show() {
        //musicaMenu.play();
        stage = new Stage();
        Table table = new Table(game.getSkin());
        table.setFillParent(true);
        table.center();

        if(die) {
            ImagenGameOverScreen = new Texture("img/gameover.jpg");
        }
        else {
            ImagenGameEndScreen = new Texture("img/endGame.jpg");
        }
        label = new Label(" ", game.getSkin());
        label.setFontScale(2.5f);
        final TextField textName = new TextField("Introduce tu nombre", game.getSkin());
        textName.setPosition(label.getOriginX(), label.getOriginY() - 350);
        textName.setWidth(200);
        textName.setHeight(40);
        Label labelScore = new Label("Tu puntuacion: " + Constants.PUNTUACION_JUGADOR, game.getSkin());
        labelScore.setPosition(label.getOriginX() + 500, label.getOriginY() - 450);
        labelScore.setWidth(200);
        labelScore.setHeight(40);
        TextButton buttonQuit = new TextButton("Ok", game.getSkin());
        buttonQuit.setPosition(label.getOriginX(), label.getOriginY() - 400);
        buttonQuit.setWidth(200);
        buttonQuit.setHeight(40);
        buttonQuit.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {

                ConfigurationManager.addScores(textName.getText(), Constants.PUNTUACION_JUGADOR);
                stage.clear();
                game.setScreen(new ScoreScreen(game));
            }
        });

        Label aboutLabel = new Label("The Run Paradise\n Ainoa Andres Burguete\nTodos los derechos reservados", game.getSkin());
        aboutLabel.setFontScale(1f);


        table.row().height(150);
        table.add(label).center().pad(35f);
        table.row().height(70);
        table.add(textName).center().width(400).pad(5f);
        table.row().height(70);
        table.add(buttonQuit).center().width(300).pad(5f);
        table.row().height(70);
        table.add(labelScore).center().width(300).pad(5f);
        table.row().height(70);
        //table.add(aboutLabel).center().pad(55f);
        //table.row().height(70);
        stage.addActor(table);

        //QUE GESTIONE LOS EVENTOS DE RATON, TECLADO, PANTALLA
        Gdx.input.setInputProcessor(stage);

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(delta);
        batch.begin();
        if(die) {
            batch.draw(ImagenGameOverScreen, 0, 0,ImagenGameOverScreen.getWidth(),ImagenGameOverScreen.getHeight());
        }
        else {
            batch.draw(ImagenGameEndScreen, 0,0,ImagenGameEndScreen.getWidth(),ImagenGameEndScreen.getHeight());
        }


        batch.end();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();

    }
}
