package com.ainochu.mygame.screens;

import com.ainochu.mygame.TheRunParadise;
import com.ainochu.mygame.managers.*;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;



public class GameScreen implements Screen {

    TheRunParadise game;
    RenderManager renderManager;
    SpriteManager spriteManager;
    LevelManager levelManager;
    CameraManager cameraManager;
    SpriteBatch batch;
    String nombrePersonaje;
    Music music;

    public GameScreen(TheRunParadise game,String name){
        this.game = game;
        this.nombrePersonaje = name;
        spriteManager = new SpriteManager(game,name);
        levelManager = new LevelManager(spriteManager);
        levelManager.cargarNivel();
        this.spriteManager.setLevelManager(this.levelManager);

        cameraManager = new CameraManager(spriteManager,levelManager);
        levelManager.setCameraManager(cameraManager);
        spriteManager.setCameraManager(cameraManager);
        batch = new SpriteBatch();
        renderManager = new RenderManager(batch,spriteManager,cameraManager);

    }

    @Override
    public void show() {

    }

    @Override
    public void render(float dt) {

        Gdx.gl20.glClearColor(0,0,0,0);
        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);

        spriteManager.update(dt);
        renderManager.drawFrame();


        if (Gdx.input.isKeyPressed(Input.Keys.ESCAPE)){
            game.setScreen(new InGameMenuScreen(game, this,spriteManager));
            if (ConfigurationManager.isSoundEnabled()) {
                this.spriteManager.music.stop();
            }
        }
    }


    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
