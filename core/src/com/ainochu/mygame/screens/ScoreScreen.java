package com.ainochu.mygame.screens;

import com.ainochu.mygame.TheRunParadise;
import com.ainochu.mygame.managers.ConfigurationManager;
import com.ainochu.mygame.util.Constants;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import java.awt.*;
import java.util.List;

/**
 * Created by Ainoa on 03/03/2016.
 */
public class ScoreScreen implements Screen{

    private Texture ImagenRankingScreen;
    SpriteBatch batch;
    TheRunParadise game;
    Stage stage;
    public ScoreScreen(TheRunParadise game){
        this.game = game;
        batch = new SpriteBatch();
        ImagenRankingScreen = new Texture("img/ranking.jpg");
    }

    private void loadScores(Table table, Stage stage, float x, float y) {

        // Lee las puntuaciones
        List<ConfigurationManager.Score> scores = ConfigurationManager.getTopScores();

        Label labelList = null;
        for (ConfigurationManager.Score score : scores) {
            labelList = new Label(score.name + " - " + score.score, game.getSkin());
            labelList.setColor(Color.PURPLE);
            labelList.setPosition(x, y);
            table.addActor(labelList);
            y -= 20;
        }
    }


    @Override
    public void show() {
        //musicaMenu.play();
        stage = new Stage();
        Table table = new Table(game.getSkin());

        Label title = new Label("", game.getSkin());
        title.setFontScale(2.5f);


        //PONER LA LISTA CON LOS SCORES
        // Crea una tabla, donde a�adiremos los elementos de men�
        table.setFillParent(true);
        table.setWidth(Constants.SCREEN_WIDTH);
        table.setHeight(Constants.SCREEN_HEIGTH);

        final Label label = new Label(" ", game.getSkin());
        label.setPosition(label.getOriginX() + 350, label.getOriginY() - 120);
        label.setColor(Color.CYAN);
        table.addActor(label);

        // Carga la lista de puntuaciones (top 10)
        loadScores(table, stage, Constants.SCREEN_WIDTH / 4, Constants.SCREEN_HEIGTH / 3);


        TextButton quickButton = new TextButton(" Volver a Jugar ", game.getSkin());
        quickButton.setColor(Color.PURPLE);
        quickButton.addListener(new ClickListener() {
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                Constants.PUNTUACION_JUGADOR = 0;
                dispose();
                game.setScreen(new GameScreen(game, Constants.NOMBRE_JUGADOR));
                //game.setScreen(new GameScreen(game));
            }
        });
        TextButton exitButton = new TextButton("  SALIR  ", game.getSkin());
        exitButton.setColor(Color.PURPLE);
        exitButton.addListener(new ClickListener() {
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                dispose();
                System.exit(0);
            }
        });



        Label aboutLabel = new Label("The Run Paradise\n Ainoa Andres Burguete\nTodos los derechos reservados", game.getSkin());
        table.add(quickButton).right().width(300).pad(10f);
        table.row().height(70);
        table.add(exitButton).right().width(300).pad(10f);
        aboutLabel.setFontScale(1f);
        table.row().height(150);
        /*

        */
        table.row().height(70);


        stage.addActor(table);

        //QUE GESTIONE LOS EVENTOS DE RATON, TECLADO, PANTALLA
        Gdx.input.setInputProcessor(stage);

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(delta);
        batch.begin();
        batch.draw(ImagenRankingScreen, 0, 0, ImagenRankingScreen.getWidth(), ImagenRankingScreen.getHeight());
        batch.end();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();

    }
}
