package com.ainochu.mygame.screens;

import com.ainochu.mygame.TheRunParadise;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/**
 * Created by Ainoa on 07/02/2016.
 */
public class MainMenuScreen implements Screen {

    TheRunParadise game;
    Stage stage;
    SpriteBatch batch;
    Texture ImagenMenuScreen;
    //Music musicaMenu;

    public MainMenuScreen(TheRunParadise game){
        this.game = game;
        batch = new SpriteBatch();
        ImagenMenuScreen = new Texture("img/SplashScreen.jpg");
        //musicaMenu = Gdx.audio.newMusic(Gdx.files.internal("musics/BSO-Armageddon-Launch.mp3"));
        //musicaMenu.setLooping(true);

    }
    @Override
    public void show() {

        //musicaMenu.play();
        stage = new Stage();
        Table table = new Table(game.getSkin());
        table.setFillParent(true);
        table.center();

        Label title = new Label("THE RUN PARADISE \nMENU PRINCIPAL", game.getSkin());
        title.setFontScale(2.5f);

        TextButton quickButton = new TextButton(" PLAY ", game.getSkin());
        quickButton.addListener(new ClickListener() {
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                dispose();
                game.setScreen(new ConfigurationScreen(game));
                //game.setScreen(new GameScreen(game));
            }
        });

        /*
        TextButton optionsButton = new TextButton("OPTIONS", game.getSkin());
        optionsButton.addListener(new ClickListener() {
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                dispose();
                game.setScreen(new ConfigurationScreen(game));

            }
        });
        */


        TextButton exitButton = new TextButton("  SALIR  ", game.getSkin());
        exitButton.addListener(new ClickListener() {
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                dispose();
                System.exit(0);
            }
        });

        Label aboutLabel = new Label("The Run Paradise\n Ainoa Andres Burguete\nTodos los derechos reservados", game.getSkin());
        aboutLabel.setFontScale(1f);


        //TODO CAMBIAR BOTONES DE SITIO
        table.row().height(150);
        table.add(title).center().pad(35f);
        table.row().height(70);
        table.add(quickButton).center().width(300).pad(5f);
        table.row().height(70);
       // table.add(optionsButton).center().width(300).pad(5f);
       // table.row().height(70);
        table.add(exitButton).center().width(300).pad(5f);
        table.row().height(70);
        table.add(aboutLabel).center().pad(55f);

        stage.addActor(table);

        //QUE GESTIONE LOS EVENTOS DE RATON, TECLADO, PANTALLA
        Gdx.input.setInputProcessor(stage);

    }

    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(delta);
        batch.begin();
        batch.draw(ImagenMenuScreen,0,0);
        batch.end();
        stage.draw();


    }

    @Override
    public void dispose() {

        //musicaMenu.dispose();
        stage.dispose();

    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        //musicaMenu.stop();

    }
}
