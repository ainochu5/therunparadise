package com.ainochu.mygame.screens;

import com.ainochu.mygame.TheRunParadise;
import com.ainochu.mygame.util.Constants;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Scaling;

/**
 * Created by Ainoa on 05/03/2016.
 */
public class InstructionScreen implements Screen {

    private Image splashImage;
    private Texture splashTexture;
    TheRunParadise game;
    Stage stage;
    SpriteBatch batch;
    Texture ImagenMenuScreen;


    public InstructionScreen(TheRunParadise game) {
        this.game = game;
        this.game = game;
        batch = new SpriteBatch();
        ImagenMenuScreen = new Texture("img/instrucciones.jpg");

    }
    @Override
    public void show() {
        stage = new Stage();

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
        batch.draw(ImagenMenuScreen,0,0,ImagenMenuScreen.getWidth(), ImagenMenuScreen.getHeight());
        batch.end();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    public void drawStage(float delta){
        stage.act(delta);
        stage.draw();
    }
}
