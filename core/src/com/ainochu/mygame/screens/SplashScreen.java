package com.ainochu.mygame.screens;

import com.ainochu.mygame.TheRunParadise;
import com.ainochu.mygame.managers.ResourceManager;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

/**
 * Created by Ainoa on 07/02/2016.
 */
public class SplashScreen implements Screen{

    private Sprite splash;
    private Texture splashTexture;
    private TheRunParadise game;
    private Image splashImage;
    private Stage stage;

    private boolean splashDone = false;

    public SplashScreen(TheRunParadise game){
        this.game = game;


        splashTexture = new Texture(Gdx.files.internal("img/SplashScreen.jpg"));
        splashImage = new Image(splashTexture);
        splash = new Sprite(splashTexture);
        splash.setSize(1024, 768);
        stage = new Stage();
    }


    public void show() {
        ResourceManager.loadAllResources();

        Table table = new Table();
        table.setFillParent(true);
        table.center();

        //EL TIEMPO QUE DURA EL SPLASH
        splashImage.addAction(Actions.sequence(Actions.alpha(0), Actions.fadeIn(1f),
                Actions.delay(1.5f), Actions.run(new Runnable() {
                    public void run() {
                        splashDone = true;
                    }
                })));

        table.row().height(splashTexture.getHeight());
        table.add(splashImage).center();
        stage.addActor(table);
    }

    public void render(float delta) {

        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act();
        stage.draw();

        if(ResourceManager.update()) {
            if (splashDone == true) {
                //AVANZAR A LA SIGUIENTE PANTALLA
                game.setScreen(new MainMenuScreen(game));
            }
        }

    }

    public void resize(int width, int height) {

    }

    public void pause() {

    }

    public void resume() {

    }

    public void hide() {

    }

    public void dispose() {

        stage.dispose();
        splashTexture.dispose();
    }
}
