package com.ainochu.mygame.managers;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;

import java.io.File;

import static com.ainochu.mygame.util.Constants.*;


public class ResourceManager {

    public static AssetManager assets = new AssetManager();

    public static void loadAllResources(){

        assets.load(TEXTURE_ATLAS, TextureAtlas.class);

        loadSounds();
        loadMusic();
    }

    public static boolean update(){
        return assets.update();
    }

    public static void loadSounds() {

        assets.load("sounds" + File.separator + "select_your_player.wav", Sound.class);
        assets.load("sounds" + File.separator + "salto.wav", Sound.class);
        assets.load("sounds" + File.separator + "personaje_principal.wav", Sound.class);
        assets.load("sounds" + File.separator + "Rebote.mp3", Sound.class);
        assets.load("sounds" + File.separator + "laser.wav", Sound.class);
        assets.load("sounds" + File.separator + "game_over.mp3", Sound.class);
        assets.load("sounds" + File.separator + "robot.wav", Sound.class);
        assets.load("sounds" + File.separator + "ninja.mp3", Sound.class);
        assets.load("sounds" + File.separator + "piece_of_heart.ogg", Sound.class);
    }
    public static void loadMusic(){
        assets.load("music" + File.separator + "escenario1.wav", Music.class);
        assets.load("music" + File.separator + "escenario2.wav", Music.class);


    }
    public static TextureRegion getRegion(String name){

        return assets.get(TEXTURE_ATLAS,TextureAtlas.class).findRegion(name);
    }

    public static TextureRegion getRegion(String name, int position){

        return assets.get(TEXTURE_ATLAS,TextureAtlas.class).findRegion(name,position);
    }

    public static Array<TextureAtlas.AtlasRegion> getRegions(String name){
        return assets.get(TEXTURE_ATLAS,TextureAtlas.class).findRegions(name);
    }

    public static Texture getTexture(String name){
        return assets.get(name,Texture.class);
    }

    public static Sound getSound(String name){
        return assets.get(name,Sound.class);
    }

    public static Music getMusic(String name){
        return assets.get(name,Music.class);
    }
}
