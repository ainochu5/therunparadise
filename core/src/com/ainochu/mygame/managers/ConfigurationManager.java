package com.ainochu.mygame.managers;

import com.ainochu.mygame.util.Constants;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Gestor de la configuración de la partida
 * @author Santiago Faci
 * @version curso 2014-2015
 */
public class ConfigurationManager {

    private static Preferences prefs = Gdx.app.getPreferences(Constants.NOMBRE_JUEGO);;

    /**
     * Comprueba si el sonido está o no activado durante el juego
     * @return
     */
    public static boolean isSoundEnabled() {

        return prefs.getBoolean("sound");
    }

    public static void addScores(String name, int score) {

        try {
            Class.forName("org.sqlite.JDBC");

            Connection connection = null;
            connection = DriverManager.getConnection("jdbc:sqlite:" + Gdx.files.internal("scores.db"));

            Statement statement = connection.createStatement();
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS scores (id integer primary key autoincrement, name text, score int)");
            statement.executeUpdate("INSERT INTO scores (name, score) VALUES ('" + name + "', " + score + ")");

            if (statement != null)
                statement.close();
            if (connection != null)
                connection.close();

        } catch (ClassNotFoundException cnfe) {
            cnfe.printStackTrace();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }
    }

    /**
     * Devuelve la lista de las diez mejores puntuaciones del juego
     * @return La lista de puntuaciones
     */
    public static List<Score> getTopScores() {

        try {
            Class.forName("org.sqlite.JDBC");

            Connection connection = null;
            connection = DriverManager.getConnection("jdbc:sqlite:" + Gdx.files.internal("scores.db"));

            Statement statement = connection.createStatement();
            ResultSet res = statement.executeQuery("SELECT name, score FROM scores ORDER BY score DESC LIMIT 10");
            List<Score> scores = new ArrayList<Score>();
            Score score = null;
            while (res.next()) {
                score = new Score();
                score.name = res.getString("name");
                score.score = res.getInt("score");
                scores.add(score);
            }

            if (statement != null)
                statement.close();
            if (res != null)
                res.close();
            if (connection != null)
                connection.close();

            return scores;

        } catch (ClassNotFoundException cnfe) {
            cnfe.printStackTrace();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }

        return new ArrayList<Score>();
    }

    public static class Score {
        public String name;
        public int score;
    }
}
