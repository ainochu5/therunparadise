package com.ainochu.mygame.managers;


import characters.*;
import characters.Character;
import characters.items.*;
import com.ainochu.mygame.TheRunParadise;
import com.ainochu.mygame.screens.EndScreen;
import com.ainochu.mygame.util.Constants;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Timer;

import static com.ainochu.mygame.util.Constants.*;

public class SpriteManager {


    TheRunParadise game;
    Player player;
    Array<Enemy>enemigoGato;
    Array<Enemy>enemigoPerro;
    Array<Laser>lasers;
    CameraManager cameraManager;
    Array<Explosion>muertes;
    Array<Moneda>coins;
    LevelManager levelManager;
    public Array<Rectangle> tiles;
    public Array<Arma> listaArmas;
    public Music music;
    private boolean muerto;
    Flecha flecha;
    private Pool<Rectangle> rectPool = new Pool<Rectangle>() {
        @Override
        protected Rectangle newObject () {
            return new Rectangle();
        }
    };
    private int contLevel;
    public Array<Verde> enemigoVerde;
    public Array<Sierra>listaSierras;
    public Array<Hielo>listaHielos;


    public Player getPlayer() {
        return player;
    }



    public SpriteManager(TheRunParadise game, String name){
        this.game = game;
        player = new Player(POSICION_INICIAL_PLAYER1_X,POSICION_INICIAL_PLAYER1_Y,name,new Vector2(0, 0));
        muertes = new Array<>();
        lasers = new Array<>();
        tiles = new Array<>();
        listaArmas = new Array<>();
        contLevel = 0;
    }

    public void setLevelManager(LevelManager levelManager) {
        this.levelManager = levelManager;
    }



    public void update(float dt){
        updateCharacters(dt);
        player.teclado(dt);
    }

    public void updateCharacters(float dt){
        if(!player.isDead()) {
            if (!game.paused) {
                //comprobarColisiones();
                player.update(dt,this);
                //player.checkCollisions(this);
                cameraManager.handleCamera();
                if (player.disparo) {
                    if (player.isRight) {
                        listaArmas.add(new Arma(player.position.x + 50, player.position.y + 50, player));
                    } else {
                        listaArmas.add(new Arma(player.position.x, player.position.y + 50, player));
                    }
                    player.disparo = false;
                }
                if(levelManager.nivelActual==4){
                    if(player.position.x >= 5000){
                        player.position.x = 5000-player.rect.getWidth();
                    }
                }
                updateEnemies(dt);
                comprobarMonedas();
                updateSierras(dt);
        }
        } else {
            if (ConfigurationManager.isSoundEnabled()) {
                ResourceManager.getSound(Constants.SOUND + "game_over.mp3").play();
                music.stop();
            }
            game.setScreen(new EndScreen(game, true,player.score));
        }

    }

    private void updateSierras(float dt) {
        for(Sierra sierra: listaSierras){
            if(sierra.rect.overlaps(player.rect)){
                if (!player.muerto) {
                    player.muerto = true;
                    Explosion explosion = new Explosion(player.rect.x, player.rect.y,
                            player.name);
                    muertes.add(explosion);
                    player.lives--;
                    player.comprobarVidas();
                    if (!player.isDead()) {
                        //HACER QUE CUANDO PASEN UNOS SEGUNDOS SE VAYA AL PRINCIPIO DE LA PANTALLA
                        Timer.schedule(new Timer.Task() {
                            @Override
                            public void run() {
                                player.position.x = player.rect.x;
                                player.position.y = player.rect.y -50;
                                player.muerto = false;
                            }
                        }, 1);
                    }
                    else{
                        return;
                    }
                }
            }
            sierra.update(dt);
        }
    }

    public void updateEnemies(float dt){
        for(Enemy enemy: enemigoGato){
            enemy.fire();
            if (cameraManager.camera.frustum.pointInFrustum(new Vector3(enemy.position.x, enemy.position.y, 0))) {
                enemy.update(dt,this);
                if (enemy.disparo) {
                    lasers.add(new Laser(enemy.position.x, enemy.position.y + 50, enemy.state));
                    enemy.disparo = false;
                }
                for (Arma arma : listaArmas) {
                    if (enemy.rect.overlaps(arma.rect)) {
                        if (enemy instanceof Gato) {
                            Explosion explosion = new Explosion(enemy.rect.x, enemy.rect.y,
                                    "gato");
                            muertes.add(explosion);
                            enemigoGato.removeValue(enemy, true);
                            listaArmas.removeValue(arma, true);

                        }
                        if (enemy instanceof Perro) {
                            Explosion explosion = new Explosion(((Perro) enemy).rect.x, ((Perro) enemy).rect.y,
                                    "perro");
                            muertes.add(explosion);
                            enemigoGato.removeValue(enemy, true);
                            listaArmas.removeValue(arma, true);

                        }

                    }
                }
            }
            for(Laser laser: lasers){
                if(laser.rect.overlaps(player.rect)) {
                    muerto = true;
                    if (!player.muerto) {
                        player.muerto = true;
                        lasers.removeValue(laser, true);
                        Explosion explosion = new Explosion(player.rect.x, player.rect.y,
                                player.name);
                        muertes.add(explosion);
                        player.lives--;
                        player.comprobarVidas();
                        if (!player.isDead()) {
                            //HACER QUE CUANDO PASEN UNOS SEGUNDOS SE VAYA AL PRINCIPIO DE LA PANTALLA
                            Timer.schedule(new Timer.Task() {
                                @Override
                                public void run() {
                                    player.position.x = Constants.POSICION_INICIAL_PLAYER1_X;
                                    player.position.y = Constants.POSICION_INICIAL_PLAYER1_Y;
                                    player.muerto = false;
                                    listaArmas.clear();
                                }
                            }, 1);
                        }
                        else{
                            return;
                        }
                    }
                }
                if(laser.contLaser == 100){
                    lasers.removeValue(laser, true);
                }
            }
        }
        for(final Enemy enemy: enemigoPerro) {
            if (enemy instanceof Perro) {
                if (cameraManager.camera.frustum.pointInFrustum(new Vector3(((Perro) enemy).position.x, ((Perro) enemy).position.y, 0))) {
                    enemy.checkCollisions(this);
                    for (Arma arma : listaArmas) {
                        if (enemy.rect.overlaps(arma.rect)) {
                            if (enemy instanceof Perro) {
                                Explosion explosion = new Explosion(enemy.rect.x, enemy.rect.y,
                                        "perro");
                                muertes.add(explosion);
                                enemigoPerro.removeValue(enemy, true);
                                listaArmas.removeValue(arma, true);
                            }
                        }
                    }
                    if (((Perro) enemy).desbloqueado) {
                        if (player.position.x < enemy.position.x) {
                            enemy.position.x--;
                            enemy.state = Character.State.LEFT;
                            ((Perro) enemy).derecha = false;
                        }
                        if (player.position.x > enemy.position.x) {
                            enemy.position.x++;
                            enemy.state = Character.State.RIGHT;
                            ((Perro) enemy).derecha = true;
                        }
                    }
                    if (player.rect.overlaps(((Perro) enemy).rect)) {
                        if (!player.muerto) {
                            player.muerto = true;
                            Explosion explosion = new Explosion(player.rect.x, player.rect.y,
                                    player.name);
                            muertes.add(explosion);
                            ((Perro) enemy).devolverPosicionInicial();
                            player.lives--;
                            player.comprobarVidas();
                            if (!player.isDead()) {
                                //HACER QUE CUANDO PASEN UNOS SEGUNDOS SE VAYA AL PRINCIPIO DE LA PANTALLA
                                Timer.schedule(new Timer.Task() {
                                    @Override
                                    public void run() {
                                        player.position.x = Constants.POSICION_INICIAL_PLAYER1_X;
                                        player.position.y = Constants.POSICION_INICIAL_PLAYER1_Y;
                                        player.muerto = false;
                                        listaArmas.clear();
                                    }
                                }, 1);
                            }
                            else{
                                return;
                            }
                        }
                        else{
                            enemigoPerro.removeValue(enemy, true);
                        }

                    }
                    enemy.update(dt,this);
                }
                else {
                    ((Perro) enemy).devolverPosicionInicial();
                }


            }

        }
        for (Explosion explosion : muertes) {
            explosion.update(dt);
            if (explosion.isDead())
                muertes.removeValue(explosion, true);
        }
        for(Verde verde: enemigoVerde){
            verde.update(dt);
            if(verde.rect.overlaps(player.rect)){
                ResourceManager.getSound(Constants.SOUND + "game_over.mp3").play();
                music.stop();
                game.setScreen(new EndScreen(game,true,player.score));
            }
        }
        actualizarArmas(dt);
    }

    public void actualizarArmas(float dt){
        actualizarLaser(dt);
        actualizarArmaPersonaje(dt);
    }

    private void actualizarArmaPersonaje(float dt) {
        for (Arma arma: listaArmas){
            if (cameraManager.camera.frustum.pointInFrustum(new Vector3(arma.posicion.x, arma.posicion.y, 0))) {
                arma.update(dt);
                if (arma.isRigth) {
                    arma.move(new Vector2(+dt, 0));
                } else {
                    arma.move(new Vector2(-dt, 0));
                }
            }
            else{
                listaArmas.removeValue(arma,true);
            }
        }
    }

    public void actualizarLaser(float dt){
        for(Laser laser: lasers){
            laser.update(dt);
            if(laser.state == Character.State.RIGHT) {
                laser.move(new Vector2(+dt, 0));
            }
            else{
                laser.move(new Vector2(-dt, 0));
            }
        }
    }

    public void comprobarMonedas(){
        for(Moneda moneda: coins){
            if(moneda.rect.overlaps(player.rect)){
                coins.removeValue(moneda,true);
                player.score+=20;
                Constants.PUNTUACION_JUGADOR +=20;
                if(player.score > 100){
                    player.score = 0;
                }
            }
        }


        if(levelManager.nivelActual!=4) {
            if (player.rect.overlaps(flecha.rect)) {
                contLevel++;
                player.spera = true;
                if (contLevel == 1) {
                    if (ConfigurationManager.isSoundEnabled()) {
                        ResourceManager.getSound(Constants.SOUND + "piece_of_heart.ogg").play();
                    }
                    Timer.schedule(new Timer.Task() {
                        @Override
                        public void run() {
                            music.stop();
                            levelManager.passNivel();
                            levelManager.playCurrentLevelMusic();
                            player.position.x = Constants.POSICION_INICIAL_PLAYER1_X;
                            player.position.y = Constants.POSICION_INICIAL_PLAYER1_Y;
                            enemigoPerro.clear();
                            enemigoGato.clear();
                            listaArmas.clear();
                            tiles.clear();
                            listaHielos.clear();
                            contLevel = 0;
                            player.spera = false;
                        }
                    }, 2);
                }


            }
        }
        else if (levelManager.nivelActual == 4){
            if (player.rect.overlaps(flecha.rect)) {
                player.spera = true;
                contLevel++;
                if (contLevel == 1) {
                    if (ConfigurationManager.isSoundEnabled()) {
                        ResourceManager.getSound(Constants.SOUND + "piece_of_heart.ogg").play();
                    }
                    Timer.schedule(new Timer.Task() {
                        @Override
                        public void run() {
                            //CARGAR LA PANTALLA DE FIN DE JUEGO
                            game.setScreen(new EndScreen(game,false,player.score));
                            if (ConfigurationManager.isSoundEnabled()) {
                                music.stop();
                            }
                            enemigoPerro.clear();
                            enemigoGato.clear();
                            listaArmas.clear();
                            coins.clear();
                            tiles.clear();
                            contLevel = 0;
                            player.spera = false;
                        }
                    }, 2);
                }


            }
        }


    }


    public void getCollisionTiles(Character character, int startX, int endX, int startY, int endY) {

        tiles.clear();

        int xCell, yCell;
        for (int y = startY; y <= endY; y++) {
            for (int x = startX; x <= endX; x++) {
                xCell = x / TILE_WIDTH;
                yCell = y / TILE_HEIGHT;
                TiledMapTileLayer.Cell cell = levelManager.collisionLayer.getCell(xCell, yCell);
                if ((cell != null) && (cell.getTile().getProperties().containsKey("blocked"))) {
                    player.saltando = false;
                    Rectangle rect = rectPool.obtain();
                    if (character.velocity.y > 0 ) {
                        if (!(cell.getTile().getProperties().get("blocked").equals("down")))
                            rect.set(x, y, 1, 1);
                        if (!(cell.getTile().getProperties().get("blocked").equals("up"))){
                            rect.set(x, y, 1, 1);
                        }
                    }
                    else
                        rect.set((int) (Math.ceil(x / TILE_WIDTH) * TILE_WIDTH),
                                (int) (Math.ceil(y / TILE_HEIGHT) * TILE_HEIGHT), TILE_WIDTH, TILE_WIDTH);

                    tiles.add(rect);

                    if (cell.getTile().getProperties().containsKey("death")) {
                        if (character instanceof Player) {
                            character.die();
                            break;
                        }
                    }
                }
                if ((cell != null) && (cell.getTile().getProperties().containsKey("sea"))){
                }
            }
        }
    }



    public void setCameraManager(CameraManager cameraManager) {
        this.cameraManager = cameraManager;
    }
    
    
    public void comprobarColisiones(){
        Rectangle rectangle = new Rectangle(0,0,0,0);
        for (Rectangle r: LevelManager.terreno){
            if (Intersector.intersectRectangles(player.rect, r, rectangle));
                colisionSolida(player, r, rectangle);
        }
    }

    private void colisionSolida(Player player, Rectangle r, Rectangle rectangle) {
        if(rectangle.y > player.position.y){
            if((player.position.y + player.rect.height) - r.y < 7f){
                if(player.velocity.y > 0){
                    if(player.position.x + (r.x + r.width) == -1f){
                        return;
                    }
                    else if ((player.position.x + player.rect.width) - r.x == 1f) {
                        return;
                    }
                    player.position.y += -rectangle.height;
                }
            }
        }
        if(rectangle.y + rectangle.height < player.position.y + player.rect.height){
            if(((r.y + r.height)-player.position.y < 7f)){
                if(player.position.x - (r.x + r.width) == -1f){
                    return;
                }
                else if ((player.position.x + player.rect.width)-r.x == 1f){
                    return;
                }
                player.position.y += rectangle.height -1f;
            }
        }
        if(rectangle.x > player.position.x)
        {
            if ((player.position.x + player.rect.width) - r.x < 7f)
            {
                if (player.position.y - (r.y + r.height) == -1f)
                    return;
                player.position.x += -rectangle.width + 1f;
            }
        }
        if(rectangle.x + rectangle.width < player.position.x + player.rect.width)
        {
            if ((r.x + r.width) - player.position.x < 7f)
            {
                if (player.position.y - (r.y + r.height) == -1f)
                    return;
                player.position.x += rectangle.width - 1f;
            }
        }

    }


}
