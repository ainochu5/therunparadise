package com.ainochu.mygame.managers;


import characters.Enemy;
import characters.Explosion;
import characters.Perro;
import characters.items.*;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import static com.ainochu.mygame.util.Constants.*;

public class RenderManager {

    SpriteManager spriteManager;
    CameraManager cameraManager;
    SpriteBatch batch;
    BitmapFont font;
    private String foto_vida;
    private String foto_puntuacion;


    public RenderManager(SpriteBatch batch,SpriteManager spriteManager,CameraManager cameraManager){
        this.spriteManager = spriteManager;
        this.cameraManager = cameraManager;
        this.batch = batch;
        font = new BitmapFont();

    }

    public void drawFrame(){

        batch.setProjectionMatrix(cameraManager.camera.combined);
        batch.begin();
        if(!spriteManager.player.isDead()) {
            if(!spriteManager.player.muerto) {
                spriteManager.player.render(batch);
            }
            drawEnemies();
            drawItems();
            for (Explosion explosion : spriteManager.muertes)
                explosion.render(batch);
        }
        drawHud();
        batch.end();
    }

    private void drawHud() {

        opcionHudVida();
        opcionHudPuntuacion();
        //VIDAS
        if(spriteManager.levelManager.nivelActual!=4) {
            batch.draw(ResourceManager.getRegion("heart_full"), cameraManager.camera.position.x - CAMERA_WIDTH / 2 + 10, CAMERA_HEIGHT - TILE_HEIGHT);
            batch.draw(ResourceManager.getRegion("x"), cameraManager.camera.position.x - CAMERA_WIDTH / 2 + 70, CAMERA_HEIGHT - TILE_HEIGHT);
            batch.draw(ResourceManager.getRegion(foto_vida), cameraManager.camera.position.x - CAMERA_WIDTH / 2 + 110, CAMERA_HEIGHT - TILE_HEIGHT);
            //PUNTOS
            batch.draw(ResourceManager.getRegion("coins"), cameraManager.camera.position.x - CAMERA_WIDTH / 2 + 190, CAMERA_HEIGHT - TILE_HEIGHT);
            batch.draw(ResourceManager.getRegion("x"), cameraManager.camera.position.x - CAMERA_WIDTH / 2 + 240, CAMERA_HEIGHT - TILE_HEIGHT);
            batch.draw(ResourceManager.getRegion(foto_puntuacion), cameraManager.camera.position.x - CAMERA_WIDTH / 2 + 280, CAMERA_HEIGHT - TILE_HEIGHT);
            //NIVEL
            font.draw(batch, " NIVEL " + spriteManager.levelManager.nivelActual, cameraManager.camera.position.x - CAMERA_WIDTH / 2 + CAMERA_WIDTH - 60, CAMERA_HEIGHT + 40 - TILE_HEIGHT);
        }
        else{
            batch.draw(ResourceManager.getRegion("heart_full"), cameraManager.camera.position.x - CAMERA_WIDTH / 2 + 10, cameraManager.camera.position.y+(TILE_HEIGHT*4));
            batch.draw(ResourceManager.getRegion("x"), cameraManager.camera.position.x - CAMERA_WIDTH / 2 + 70, cameraManager.camera.position.y+(TILE_HEIGHT*4));
            batch.draw(ResourceManager.getRegion(foto_vida), cameraManager.camera.position.x - CAMERA_WIDTH / 2 + 110, cameraManager.camera.position.y+(TILE_HEIGHT*4));
            //PUNTOS
            batch.draw(ResourceManager.getRegion("coins"), cameraManager.camera.position.x - CAMERA_WIDTH / 2 + 190, cameraManager.camera.position.y+(TILE_HEIGHT*4));
            batch.draw(ResourceManager.getRegion("x"), cameraManager.camera.position.x - CAMERA_WIDTH / 2 + 240, cameraManager.camera.position.y+(TILE_HEIGHT*4));
            batch.draw(ResourceManager.getRegion(foto_puntuacion), cameraManager.camera.position.x - CAMERA_WIDTH / 2 + 280, cameraManager.camera.position.y+(TILE_HEIGHT*4));
            //NIVEL
            font.draw(batch, " NIVEL " + spriteManager.levelManager.nivelActual, cameraManager.camera.position.x - CAMERA_WIDTH / 2 + CAMERA_WIDTH - 60, cameraManager.camera.position.y+(TILE_HEIGHT*4));
        }
    }

    private String opcionHudVida(){
        switch (spriteManager.player.lives){
            case 5:
                foto_vida = "5";
                break;
            case 4:
                foto_vida = "4";
                break;
            case 3:
                foto_vida = "3";
                break;
            case 2:
                foto_vida = "2";
                break;
            case 1:
                foto_vida = "1";
                break;
            default:
                break;
        }
        return foto_vida;
    }
    private String opcionHudPuntuacion(){
        switch (spriteManager.player.score){
            case 0:
                foto_puntuacion = "0";
                break;
            case 20:
                foto_puntuacion = "20";
                break;
            case 40:
                foto_puntuacion = "40";
                break;
            case 60:
                foto_puntuacion = "60";
                break;
            case 80:
                foto_puntuacion = "80";
                break;
            case 100:
                foto_puntuacion = "100";
                break;
            default:
                break;
        }
        return foto_vida;
    }

    public void drawItems(){
        for(Laser laser: spriteManager.lasers){
            laser.render(batch);
        }
        for(Moneda moneda:spriteManager.coins){
            moneda.render(batch);
        }
        for(Arma arma: spriteManager.listaArmas){
            arma.render(batch);
        }
        for(Hielo hielo: spriteManager.listaHielos){
            hielo.render(batch);
        }
    }

    public void drawEnemies(){
        pintarGatos();
        pintarPerros();
        pintarVerde();
        pintarSierras();
        if(spriteManager.levelManager.nivelActual!=4) {
            spriteManager.flecha.render(batch);
        }
    }

    public void pintarGatos(){
        for(Enemy enemy:spriteManager.enemigoGato){
            enemy.render(batch);
        }
    }

    public void pintarPerros(){
        for(Enemy enemy: spriteManager.enemigoPerro){
            if(!enemy.isDead()) {
                enemy.render(batch);
            }
        }
    }
    public void pintarVerde(){
        for (Verde verde: spriteManager.enemigoVerde){
            verde.render(batch);
        }
    }
    public void pintarSierras(){
        for(Sierra sierra: spriteManager.listaSierras){
            sierra.render(batch);
        }
    }


}
