package com.ainochu.mygame.managers;

import com.badlogic.gdx.graphics.OrthographicCamera;
import static com.ainochu.mygame.util.Constants.*;

public class CameraManager {

    SpriteManager spriteManager;
    LevelManager levelManager;
    OrthographicCamera camera;

    public CameraManager(SpriteManager spriteManager, LevelManager levelManager) {
        this.spriteManager = spriteManager;
        this.levelManager = levelManager;
        init();
    }

    public void init() {
        camera = new OrthographicCamera();
        camera.setToOrtho(false, TILES_IN_CAMERA * TILE_WIDTH, TILES_IN_CAMERA * TILE_HEIGHT);
        camera.update();
    }

    public void handleCamera() {

        camera.position.set(spriteManager.player.rect.x, spriteManager.player.rect.y,0);
        if(camera.position.x < 0 + camera.viewportWidth / 2) {
            camera.position.x  = 0 + camera.viewportWidth / 2;
        }
        if (camera.position.x  > LevelManager.TOTAL_WIDTH - camera.viewportWidth / 2) {
            camera.position.x  = LevelManager.TOTAL_WIDTH - camera.viewportWidth / 2;
        }
        if(camera.position.y < 0 + camera.viewportHeight / 2) {
            camera.position.y = 0 + camera.viewportHeight / 2;
        }
        if(camera.position.y> LevelManager.TOTAL_HEIGHT - camera.viewportHeight / 2) {
            camera.position.y = LevelManager.TOTAL_HEIGHT - camera.viewportHeight / 2;
        }
        camera.update();

        levelManager.mapRenderer.setView(camera);
        levelManager.mapRenderer.render();
    }
}
