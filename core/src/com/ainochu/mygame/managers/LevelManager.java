package com.ainochu.mygame.managers;


import characters.Enemy;
import characters.items.*;
import characters.Gato;
import characters.Perro;
import com.ainochu.mygame.util.Constants;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.objects.TextureMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;

public class LevelManager {

    public int nivelActual = 1;
    TiledMap map;
    TiledMapTileLayer collisionLayer;
    SpriteManager spriteManager;
    CameraManager cameraManager;
    OrthogonalTiledMapRenderer mapRenderer;
    SpriteBatch batch;
    MapLayer collision;
    public static float TOTAL_WIDTH;
    public static float TOTAL_HEIGHT;
    private int speed;
    public Flecha flecha;
    private TiledMapTileLayer extras;
    public static Array<Rectangle> terreno = new Array<>();

    public LevelManager(SpriteManager spriteManager){

        this.spriteManager = spriteManager;
        batch = new SpriteBatch();
    }

    public void setCameraManager(CameraManager cameraManager) {
        this.cameraManager = cameraManager;
    }

    public void cargarNivel(){
        spriteManager.enemigoGato = new Array<>();
        spriteManager.enemigoPerro = new Array<>();
        spriteManager.enemigoVerde = new Array<>();
        spriteManager.listaSierras = new Array<>();
        spriteManager.listaHielos = new Array<>();
        spriteManager.coins = new Array<>();

        map = new TmxMapLoader().load("levels/"+"escenario"+nivelActual+".tmx");
        TOTAL_WIDTH = Float.parseFloat(map.getProperties().get("tilewidth").toString())*Float.parseFloat(map.getProperties().get("width").toString());
        TOTAL_HEIGHT = Float.parseFloat(map.getProperties().get("tileheight").toString())*Float.parseFloat(map.getProperties().get("height").toString());
        //CARGAR MIS COLISIONES DE BALDOSAS
        collisionLayer = (TiledMapTileLayer) map.getLayers().get("Terreno");
        //malos = map.getLayers().get("objetos");
        mapRenderer = new OrthogonalTiledMapRenderer(map);
        collision = map.getLayers().get("objects");
        cargarEnemigos();
        cargaritems();
        terreno.clear();
        //loadTerreno();
        playCurrentLevelMusic();
    }

    public void cargarEnemigos() {
        Enemy enemy = null;
        // Carga los objetos "enemigo" del TiledMap
        for (MapObject object : map.getLayers().get("objetos").getObjects()) {
            if (object instanceof TextureMapObject) {
                TextureMapObject rectangleObject = (TextureMapObject) object;
                if (rectangleObject.getProperties().containsKey("cat")) {
                    System.out.println("Miau");
                    String enemyType = (String) rectangleObject.getProperties().get("cat");
                    switch (enemyType){
                        case "gato":
                            speed = Integer.parseInt((String) rectangleObject.getProperties().get("velocidad"));
                            int offset = Integer.parseInt((String) rectangleObject.getProperties().get("desplazamiento"));
                            enemy = new Gato(rectangleObject.getX(), rectangleObject.getY(),"gato",speed,offset);
                            spriteManager.enemigoGato.add(enemy);
                            break;
                        default:
                            break;
                    }
                }
                else if (rectangleObject.getProperties().containsKey("dog")) {
                    String enemyType = (String) rectangleObject.getProperties().get("dog");
                    switch (enemyType){
                        case "perro":
                            speed = Integer.parseInt((String) rectangleObject.getProperties().get("velocidad"));
                            int offset = Integer.parseInt((String) rectangleObject.getProperties().get("desplazamiento"));
                            enemy = new Perro(rectangleObject.getX(), rectangleObject.getY(),"perro",speed,offset);
                            spriteManager.enemigoPerro.add(enemy);
                            break;
                        default:
                            break;
                    }
                }

                else if (rectangleObject.getProperties().containsKey("cosa")){
                    String enemyType = (String) rectangleObject.getProperties().get("cosa");
                    switch (enemyType){
                        case "verde":
                            int offset = Integer.parseInt((String) rectangleObject.getProperties().get("desplazamiento"));
                            Verde verde = new Verde(rectangleObject.getX(), rectangleObject.getY(),offset);
                            spriteManager.enemigoVerde.add(verde);
                            break;
                        case "rodar":
                            Sierra sierra = new Sierra(rectangleObject.getX(), rectangleObject.getY());
                            spriteManager.listaSierras.add(sierra);
                        default:
                            break;
                    }
                }

                if(rectangleObject.getProperties().containsKey("flecha")){
                    spriteManager.flecha = new Flecha(rectangleObject.getX(),rectangleObject.getY(),nivelActual);
                }

            }
        }

        System.out.println(spriteManager.enemigoGato.size);
    }


    public void cargaritems(){
        Moneda moneda = null;
        // Carga los objetos "item" del TiledMap
        for (MapObject object : map.getLayers().get("items").getObjects()) {
            if (object instanceof TextureMapObject) {
                TextureMapObject rectangleObject = (TextureMapObject) object;
                if (rectangleObject.getProperties().containsKey("moneda")) {
                    String enemyType = (String) rectangleObject.getProperties().get("moneda");
                    switch (enemyType){
                        case "coins":
                            int puntos = Integer.parseInt((String) rectangleObject.getProperties().get("puntos"));
                            moneda = new Moneda(rectangleObject.getX(),rectangleObject.getY(),puntos);
                            spriteManager.coins.add(moneda);
                            break;
                        default:
                            break;
                    }
                }
                else if (rectangleObject.getProperties().containsKey("hielo")) {
                    String enemyType = (String) rectangleObject.getProperties().get("hielo");
                    switch (enemyType){
                        case "fin":
                            String posicion = (String) rectangleObject.getProperties().get("posicion");
                            Hielo hielo = new Hielo(rectangleObject.getX(),rectangleObject.getY(),posicion);
                            spriteManager.listaHielos.add(hielo);
                            break;
                        default:
                            break;
                    }
                }
            }
        }
    }


    public void playCurrentLevelMusic() {
        // Musica de fondo durante el juego
        if(nivelActual == 1) {
            if (ConfigurationManager.isSoundEnabled()) {
                spriteManager.music = ResourceManager.getMusic(Constants.MUSIC + "escenario1.wav");
                spriteManager.music.setLooping(true);
                spriteManager.music.setVolume(.2f);
                spriteManager.music.play();

                ResourceManager.getSound(Constants.SOUND + "personaje_principal.wav").play();
            }
        }

        else if (nivelActual == 4) {
            if (ConfigurationManager.isSoundEnabled()) {
                spriteManager.music = ResourceManager.getMusic(Constants.MUSIC + "escenario2.wav");
                spriteManager.music.setLooping(true);
                spriteManager.music.setVolume(.2f);
                spriteManager.music.play();

                ResourceManager.getSound(Constants.SOUND + "personaje_principal.wav").play();
            }
        }
    }

    public void passNivel(){
        nivelActual = 4;
        cargarNivel();
    }

    public void loadTerreno(){

        for (MapObject layer: collision.getObjects()){

            if(layer instanceof RectangleMapObject){
                terreno.add(((RectangleMapObject)layer).getRectangle());
            }
        }
    }




}
