package com.ainochu.mygame.util;

/**
 * Created by Guillermo on 16/02/2016.
 */
public class Constants {

    public static final int SCREEN_WIDTH = 1024;
    public static final int SCREEN_HEIGTH = 768;
    public static final String TEXTURE_ATLAS = "texturas/juego.pack";
    public static final float POSICION_INICIAL_PLAYER1_X = 100;
    public static final float POSICION_INICIAL_PLAYER1_Y = 200;
    public static final int TILES_IN_CAMERA = 10;
    public static final int TILE_WIDTH = 100;
    public static final int TILE_HEIGHT = 100;
    public static final float VELOCIDAD_SALTO = 10000f;
    public static final float CAMERA_WIDTH = TILES_IN_CAMERA*TILE_WIDTH;
    public static final float CAMERA_HEIGHT = TILES_IN_CAMERA*TILE_HEIGHT;
    public static final String NOMBRE_JUEGO = "TheRunParadise";
    public static final float GRAVEDAD = 300f;
    public static final String SOUND = "sounds/";
    public static final String MUSIC = "music/";
    public static final int PANTALLA = TILE_HEIGHT * 15;
    public static String NOMBRE_JUGADOR = "vaquera";
    public static int PUNTUACION_JUGADOR = 0;
}
