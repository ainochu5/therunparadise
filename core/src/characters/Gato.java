package characters;

import com.ainochu.mygame.managers.ResourceManager;
import com.ainochu.mygame.managers.SpriteManager;
import com.ainochu.mygame.util.Constants;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

import java.util.Random;

/**
 * Created by Ainoa on 14/02/2016.
 */
public class Gato extends Enemy{

    private int speed;
    private int distancia;
    private float posicionInicial;
    public boolean derecha;
    public int conttiempoBalas;

    public Gato(float x, float y, String name,
                       int speed, int offset) {
        super(x, y, name,new Vector2(0, 0));
        this.speed = speed;
        this.distancia = offset;
        posicionInicial = x;
        derecha = true;

    }

    @Override
    public void fire() {
        Random r = new Random();
        int numero = r.nextInt(100);
        if(numero == 5 && conttiempoBalas > 50){
            disparo = true;
            conttiempoBalas = 0;
        }
        else{
            conttiempoBalas++;
        }
        super.fire();
    }

    @Override
    public void update(float dt,SpriteManager spriteManager) {
        super.update(dt,spriteManager);

        if (derecha) {
            state = State.RIGHT;
            position.x += speed * dt;
            if (position.x >= posicionInicial + distancia)
                derecha = false;
        }
        else {
            state = State.LEFT;
            position.x -= speed * dt;
            if (position.x <= posicionInicial)
                derecha = true;
        }
    }





}
