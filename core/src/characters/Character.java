package characters;

import com.ainochu.mygame.managers.ResourceManager;
import com.ainochu.mygame.managers.SpriteManager;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import static com.ainochu.mygame.util.Constants.*;
/**
 * Created by Ainoa on 19/02/2016.
 */
public class Character {


    float stateTime;
    Animation leftAnimation, rightAnimation,deadLeftAnimation,deadRigthAnimation,jumpRigthAnimation,jumpLeftAnimtion,
            atackLeftAnimation,atackRigthAnimation,idleLeftAnimation,idleRightAnimation ;
    TextureRegion currentFrame;
    public Rectangle rect;
    boolean dead;
    boolean isRight;

    public enum State {
        RIGHT,LEFT, SHOOT_RIGHT, IDLE_RIGHT, SHOOT_LEFT, IDLE_LEFT,
    }
    public State state;

    public Vector2 position = new Vector2();
    public Vector2 velocity = new Vector2();

    public Character(float x, float y, String nombre,Vector2 velocity ) {

        idleRightAnimation = new Animation(0.10f,ResourceManager.getRegions(nombre+"_idle_derecha"));
        idleLeftAnimation = new Animation(0.10f,ResourceManager.getRegions(nombre+"_idle_izquierda"));
        leftAnimation = new Animation(0.10f,ResourceManager.getRegions(nombre+"_andar_izquierda"));
        rightAnimation = new Animation(0.10f,ResourceManager.getRegions(nombre+"_andar_derecha"));
        deadRigthAnimation = new Animation(0.20f,ResourceManager.getRegions(nombre+"_muerte_derecha"));
        deadLeftAnimation = new Animation(0.20f,ResourceManager.getRegions(nombre+"_muerte_izquierda"));
        atackLeftAnimation = new Animation(0.20f,ResourceManager.getRegions(nombre+"_ataque_izquierda"));
        atackRigthAnimation = new Animation(0.20f,ResourceManager.getRegions(nombre+"_ataque_derecha"));

        currentFrame = idleRightAnimation.getKeyFrame(0);

        position.x = x;
        position.y = y;

        rect = new Rectangle(x, y,
                currentFrame.getRegionWidth(),
                currentFrame.getRegionHeight());

        state = State.IDLE_RIGHT;
    }

    public void update(float dt,SpriteManager spriteManager) {
        stateTime += dt;

        switch (state) {
                case RIGHT:
                    currentFrame = rightAnimation.getKeyFrame(stateTime, true);
                    break;
                case SHOOT_RIGHT:
                    currentFrame = atackRigthAnimation.getKeyFrame(stateTime, true);
                    break;
                case IDLE_RIGHT:
                    currentFrame = idleRightAnimation.getKeyFrame(stateTime, true);
                    break;
                case LEFT:
                    currentFrame = leftAnimation.getKeyFrame(stateTime, true);
                    break;
                case SHOOT_LEFT:
                    currentFrame = atackLeftAnimation.getKeyFrame(stateTime, true);
                    break;
                case IDLE_LEFT:
                    currentFrame = idleLeftAnimation.getKeyFrame(stateTime, true);
                    break;
                default:
                    state = State.IDLE_RIGHT;
                    break;
            }


        if (position.x <0){
            position.x = 0;
        }
    }

    public void render(Batch batch) {

        batch.draw(currentFrame, position.x, position.y,
                rect.getWidth(), rect.getHeight());
    }

    public void die() {
        dead = true;
    }

    public boolean isDead() {
        return dead;
    }


    public void checkCollisions(SpriteManager spriteManager) {

        int startY, endY, startX, endX;
        // Comprueba las colisiones en el eje Y
        if (velocity.y > 0)
            startY = endY = (int) (position.y + rect.getHeight() + velocity.y);
        else
            startY = endY = (int) (position.y + velocity.y);

        startX = (int) position.x;
        endX = (int) (position.x + rect.getWidth());

        spriteManager.getCollisionTiles(this, startX, endX, startY, endY);
        rect.y += velocity.y;
        for (Rectangle tile : spriteManager.tiles) {
            if (rect.overlaps(tile)) {

                if (velocity.y > 0) {
                    position.y = tile.y - rect.getHeight();
                } else {
                    position.y = tile.y + TILE_HEIGHT;
                }
                velocity.y = 0;
                break;
            }
        }

        // Comprueba las colisiones en el eje X
        if (velocity.x > 0)
            startX = endX = (int) (position.x + rect.getWidth() + velocity.x);
        else
            startX = endX = (int) (position.x + velocity.x);

        startY = (int) position.y;
        endY = (int) (position.y + rect.getHeight());

        spriteManager.getCollisionTiles(this, startX, endX, startY, endY);
        rect.x += velocity.x;
        for (Rectangle tile : spriteManager.tiles) {
            if (rect.overlaps(tile)) {
                if (this instanceof Enemy) {
                    velocity.x = -velocity.x;
                }
                else {
                    velocity.x = 0;
                    System.out.println("parado");
                }

                break;
            }
        }
        rect.x = position.x;
    }


}
