package characters;

import com.ainochu.mygame.managers.SpriteManager;
import com.ainochu.mygame.util.Constants;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Ainoa on 22/02/2016.
 */
public class Perro extends Enemy {

    private int speed;
    private float posicionInicial;
    private int distancia;
    public boolean derecha;
    public boolean desbloqueado;

    public Perro(float x, float y, String name, int velocidad, int posicionInicial) {
        super(x, y, name,new Vector2(0, 0));
        this.speed = velocidad;
        this.distancia = posicionInicial;
        this.posicionInicial = x;
        derecha = true;
        desbloqueado = true;
    }

    @Override
    public void update(float dt,SpriteManager spriteManager) {
        if(derecha) {
            if (atackRigthAnimation.isAnimationFinished(dt)) {
                desbloqueado = true;
            }
        }
        else {
            if (atackLeftAnimation.isAnimationFinished(dt)) {
                desbloqueado = true;
            }
        }
        velocity.y -= Constants.GRAVEDAD;
        velocity.scl(dt);
        rect.set(position.x, position.y, rect.getWidth(), rect.getHeight());
        position.add(velocity);

        if ((position.y < 0) && (!isDead()))
            die();

        super.update(dt,spriteManager);

    }

    public void devolverPosicionInicial(){
        position.x = posicionInicial;
        rect.set(position.x, position.y, rect.getWidth(), rect.getHeight());
    }


}
