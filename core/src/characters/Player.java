package characters;

import com.ainochu.mygame.managers.ConfigurationManager;
import com.ainochu.mygame.managers.ResourceManager;
import com.ainochu.mygame.managers.SpriteManager;
import com.ainochu.mygame.screens.GameScreen;
import com.ainochu.mygame.util.Constants;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Ainoa on 19/02/2016.
 */
public class Player extends Character{

    public static float SPEED = 500f;
    public int lives;
    public int score;
    public boolean isRight;
    public boolean saltando;
    public boolean disparo;
    public String name;
    public boolean gravedad = false;
    private float tiempo_actual_salto;
    public float tiempo_salto = 3F;
    public boolean muerto;
    public boolean spera;


    public Player(float x, float y, String name,Vector2 velocity) {
        super(x, y, name,velocity);
        lives = 3;
        score = 0;
        this.name = name;
        muerto = false;
        jumpLeftAnimtion = new Animation(0.20f, ResourceManager.getRegions(this.name + "_salto_izquierda"));
        jumpRigthAnimation = new Animation(0.25f,ResourceManager.getRegions(this.name+"_salto_derecha"));
        spera = false;
    }

    public void fire() {
        disparo = true;
        if(name.equals("vaquera")) {
            if (ConfigurationManager.isSoundEnabled()) {
                ResourceManager.getSound(Constants.SOUND + "Rebote.mp3").play();
            }
        }
        else if (name.equals("ninja")){
            if (ConfigurationManager.isSoundEnabled()) {
                ResourceManager.getSound(Constants.SOUND + "ninja.mp3").play();
            }
        }
        else if (name.equals("robot")){
            if (ConfigurationManager.isSoundEnabled()) {
                ResourceManager.getSound(Constants.SOUND + "robot.wav").play();
            }
        }
    }

    @Override
    public void die() {
        super.die();
        lives--;
    }

    public void resurrection(){
        lives = 3;
    }

    public void teclado(float dt) {
        if(!muerto && !spera) {
            if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
                velocity.x = +SPEED;
                state = State.RIGHT;
                isRight = true;
            }
            if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
                velocity.x = -SPEED;
                state = State.LEFT;
                isRight = false;
            }

            if (Gdx.input.isKeyJustPressed(Input.Keys.A) && !isRight) {
                state = State.SHOOT_LEFT;
                isRight = false;
                fire();
            }

            if (Gdx.input.isKeyJustPressed(Input.Keys.A) && isRight) {
                state = State.SHOOT_RIGHT;
                isRight = true;
                fire();

            }
            if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE)) {
                if (!saltando)
                    jump(dt);
            }
            if (!Gdx.input.isKeyPressed(Input.Keys.RIGHT) && !Gdx.input.isKeyPressed(Input.Keys.LEFT) &&
                    !Gdx.input.isKeyPressed(Input.Keys.A) && !Gdx.input.isKeyPressed(Input.Keys.SPACE)) {
                if (isRight) {
                    state = State.IDLE_RIGHT;
                    isRight = true;
                } else {
                    state = State.IDLE_LEFT;
                    isRight = false;
                }
            }
        }
    }

    public void jump(float dt) {
        saltando = true;
        if (velocity.y == 0) {
            velocity.x += 50;
            velocity.y = Constants.VELOCIDAD_SALTO;
            currentFrame = ResourceManager.getRegion(name + "_andar_derecha", 1);
            if (ConfigurationManager.isSoundEnabled()) {
                //ResourceManager.getSound(Constants.SOUND + "salto.wav").play();
            }
        }

    }

    @Override
    //NO ENTRA AQUI
    public void update(float dt,SpriteManager spriteManager) {
        velocity.y -= Constants.GRAVEDAD;
        velocity.scl(dt);
        if (!isDead())
            checkCollisions(spriteManager);
        rect.set(position.x, position.y, rect.getWidth(), rect.getHeight());
        position.add(velocity);

        if (position.x < 0)
            position.x = 0;
        if (position.y == 0){
            saltando = false;
        }

        if ((position.y < 0) && (!isDead()))
            die();
        super.update(dt,spriteManager);
    }

    public void comprobarVidas(){
        if(lives==0){
            die();
        }
    }


    public void checkCollisions(SpriteManager spriteManager) {
        if (!isDead())
            super.checkCollisions(spriteManager);
    }

}
