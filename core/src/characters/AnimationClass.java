package characters;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Ainoa on 11/02/2016.
 * Clase que se utiliza para cualquier cosa que se mueva dentro del juego
 */
public abstract class AnimationClass {

    public Vector2 position;
    public float stateTime;
    public TextureRegion regionActual;
    protected boolean dead;
    public Rectangle rect;

    public AnimationClass(float x, float y) {

        position = new Vector2(x, y);
        rect = new Rectangle();
    }

    public void render(SpriteBatch batch) {
        if (regionActual != null)
            batch.draw(regionActual, position.x, position.y);
    }

    public abstract void update(float dt);

    public boolean isDead() {
        return dead;
    }

    public abstract void die();

    public abstract void resurrect();
}
