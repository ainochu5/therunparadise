package characters.items;

import com.ainochu.mygame.managers.ResourceManager;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Ainoa on 18/02/2016.
 */
public class Moneda {

    public int puntos;
    TextureRegion currentFrame;
    public Rectangle rect;
    public Vector2 posicion;
    float stateTime;
    private boolean isRigth;
    public Animation coinAnimation;

    public Moneda(float x, float y, int puntos){
        this.puntos = puntos;
        posicion = new Vector2(x,y);
        coinAnimation = new Animation(0.25f, ResourceManager.getRegion("coins"));
        currentFrame = coinAnimation.getKeyFrame(0,true);
        isRigth = true;
        rect = new Rectangle(posicion.x, posicion.y,currentFrame.getRegionWidth(),currentFrame.getRegionHeight());

    }
    public void incrementar(){
        puntos+=puntos;
    }

    public void render(SpriteBatch batch){
        batch.draw(currentFrame, posicion.x, posicion.y);
    }

    public void update(float dt){
        rect.set(posicion.x, posicion.y, rect.getWidth(), rect.getHeight());
        System.out.println(rect.x);
    }
}
