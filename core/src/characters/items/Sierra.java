package characters.items;

import com.ainochu.mygame.managers.ResourceManager;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Ainoa on 04/03/2016.
 */
public class Sierra {

    public int speed;
    TextureRegion currentFrame;
    Animation sierraAnimation;
    public Vector2 posicion;
    public Rectangle rect;
    private float stateTime;

    public Sierra(float x, float y) {
        posicion = new Vector2(x,y);
        sierraAnimation = new Animation(0.10f, ResourceManager.getRegions("sierra"));
        currentFrame = sierraAnimation.getKeyFrame(0,true);
        rect = new Rectangle(posicion.x, posicion.y,currentFrame.getRegionWidth(),currentFrame.getRegionHeight());
    }

    public void update(float dt) {
        stateTime+=dt;
        //MOVER LA MASA PARA ARRIBA
        currentFrame = sierraAnimation.getKeyFrame(stateTime,true);
        rect.y = posicion.y;
        rect.x = posicion.x;
    }

    public void render(SpriteBatch batch){
        batch.draw(currentFrame, posicion.x, posicion.y);
    }
}
