package characters.items;

import characters.Enemy;
import com.ainochu.mygame.managers.ResourceManager;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Ainoa on 04/03/2016.
 */
public class Verde {

    public int speed;
    TextureRegion currentFrame;
    Animation verdeAnimation;
    public Vector2 posicion;
    public Rectangle rect;

    public Verde(float x, float y,int speed) {
        posicion = new Vector2(x,y);
        verdeAnimation = new Animation(0.25f, ResourceManager.getRegion("verde"));
        currentFrame = verdeAnimation.getKeyFrame(0,true);
        this.speed = speed;
        rect = new Rectangle(posicion.x, posicion.y,currentFrame.getRegionWidth(),currentFrame.getRegionHeight());
    }

    public void update(float dt) {
        //MOVER LA MASA PARA ARRIBA
        posicion.y += 1;
        System.out.println("POSICION DE Y VERDE  " + posicion.y);
        rect.y = posicion.y;
        rect.x = posicion.x;
    }

    public void render(SpriteBatch batch){
        batch.draw(currentFrame, posicion.x, posicion.y);
    }
}
