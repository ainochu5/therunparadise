package characters.items;

import com.ainochu.mygame.managers.ResourceManager;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Ainoa on 05/03/2016.
 */
public class Hielo {


    TextureRegion currentFrame;
    Animation hieloAnimation;
    public Vector2 posicion;
    public Rectangle rect;
    public boolean isRight;

    public Hielo(float x, float y,String position) {
        posicion = new Vector2(x,y);
        hieloAnimation = new Animation(0.15f, ResourceManager.getRegions("cristal"));
        currentFrame = hieloAnimation.getKeyFrame(0,true);
        rect = new Rectangle(posicion.x, posicion.y,currentFrame.getRegionWidth(),currentFrame.getRegionHeight());
        if(position.equalsIgnoreCase("izquierda")){
            isRight = false;
        }
        else if (position.equalsIgnoreCase("derecha")){
            isRight = true;
        }
    }

    public void render(SpriteBatch batch){
        batch.draw(currentFrame, posicion.x, posicion.y);
    }
}
