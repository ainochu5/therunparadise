package characters.items;

import characters.*;
import characters.Character;
import com.ainochu.mygame.managers.ResourceManager;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Ainoa on 01/03/2016.
 */
public class Arma {

    public static float VELOCIDAD = 300f;
    TextureRegion currentFrame;
    public Rectangle rect;
    public Vector2 posicion;
    float stateTime;
    public boolean isRigth;
    public Animation bulletRight, bulletLeft;
    public characters.Character.State state;


    public Arma(float x, float y, Player player){
        posicion = new Vector2(x,y);
        bulletRight = new Animation(0.25f, ResourceManager.getRegion(player.name+"_derecha"));
        bulletLeft = new Animation(0.25f,ResourceManager.getRegion(player.name+"_izquierda"));
        this.state = state;
        if(player.isRight) {
            currentFrame = bulletRight.getKeyFrame(0, true);
            isRigth = true;

        }
        else{
            currentFrame = bulletLeft.getKeyFrame(0, true);
            isRigth = false;
        }
        rect = new Rectangle(posicion.x, posicion.y,currentFrame.getRegionWidth(),currentFrame.getRegionHeight());

    }

    public void move(Vector2 movement){
        movement.scl(VELOCIDAD);
        posicion.add(movement);
    }

    public void render(SpriteBatch batch){
        batch.draw(currentFrame, posicion.x, posicion.y);
    }

    public void update(float dt){
        rect.set(posicion.x, posicion.y, rect.getWidth(), rect.getHeight());
        System.out.println(rect.x);
    }
}
