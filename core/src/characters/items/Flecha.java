package characters.items;

import com.ainochu.mygame.managers.ResourceManager;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Ainoa on 03/03/2016.
 */
public class Flecha {

    TextureRegion currentFrame;
    public Rectangle rect;
    public Vector2 posicion;
    public Animation flechaAnimation;
    public int nivel;

    public Flecha(float x, float y,int nivel) {

        posicion = new Vector2(x,y);
        if(nivel == 1) {
            flechaAnimation = new Animation(0.25f, ResourceManager.getRegion("SignArrow"));
        }
        else if (nivel == 4){
            flechaAnimation = new Animation(0.25f, ResourceManager.getRegion("puerta"));
        }
        currentFrame = flechaAnimation.getKeyFrame(0,true);
        rect = new Rectangle(posicion.x, posicion.y,currentFrame.getRegionWidth(),currentFrame.getRegionHeight());

    }

    public void render(SpriteBatch batch){
        batch.draw(currentFrame, posicion.x, posicion.y);
    }

    public void update(float dt){
        rect.set(posicion.x, posicion.y, rect.getWidth(), rect.getHeight());
        System.out.println(rect.x);
    }
}
