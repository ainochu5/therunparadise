package com.ainochu.mygame.desktop;

import com.ainochu.mygame.TheRunParadise;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "ostras";
		config.width = 1024;
		config.height = 768;
		new LwjglApplication(new TheRunParadise(), config);
	}
}
