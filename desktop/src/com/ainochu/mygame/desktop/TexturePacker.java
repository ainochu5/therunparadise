package com.ainochu.mygame.desktop;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.tools.imagepacker.TexturePacker2;

/**
 * Created by Ainoa on 02/02/2016.
 */
public class TexturePacker {

    public static void main(String args[]){
        TexturePacker2.Settings settings = new TexturePacker2.Settings();
        settings.maxWidth = 8192;
        settings.maxHeight = 8192;
        settings.filterMag = Texture.TextureFilter.Linear;
        settings.filterMin = Texture.TextureFilter.Linear;

        TexturePacker2.process(settings, "core/assets/texturas", "core/assets/texturas", "juego.pack");
    }
}
